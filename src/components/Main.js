import React from 'react'
import { Switch, Route } from 'react-router-dom'
import Home from './Home'
import User from './User'
import Statistiks from './Statistiks'

const Main = () => (
  <main>
    <Switch>
      <Route exact path='/' component={Home}/>
      <Route path='/user' component={User}/>
      <Route path='/statistiks' component={Statistiks}/>
    </Switch>
  </main>
)

export default Main