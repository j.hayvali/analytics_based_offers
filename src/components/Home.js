import React, { Component } from 'react';
import jd from './jd.png';
import { Link } from 'react-router-dom'
import './Home.css';

class Home extends Component {

  render() {
    return (
      <table className="homeTable">
        <tr>
          <td><Link to='/statistiks'><img src={jd} alt="user_picture" height="100" width="100" /></Link></td>
          <td>John Doe</td>
        </tr>
        <tr>
          <td><Link to='/statistiks'><img src={jd} alt="user_picture" height="100" width="100" /></Link></td>
          <td>John Doe</td>
        </tr>
        <tr>
          <td><Link to='/statistiks'><img src={jd} alt="user_picture" height="100" width="100" /></Link></td>
          <td>John Doe</td>
        </tr>
        <tr>
          <td><Link to='/statistiks'><img src={jd} alt="user_picture" height="100" width="100" /></Link></td>
          <td>John Doe</td>
        </tr>
        <tr>
          <td><Link to='/statistiks'><img src={jd} alt="user_picture" height="100" width="100" /></Link></td>
          <td>John Doe</td>
        </tr>
        <tr>
          <td><Link to='/statistiks'><img src={jd} alt="user_picture" height="100" width="100" /></Link></td>
          <td>John Doe</td>
        </tr>
        <tr>
          <td><Link to='/statistiks'><img src={jd} alt="user_picture" height="100" width="100" /></Link></td>
          <td>John Doe</td>
        </tr>
        <tr>
          <td><Link to='/statistiks'><img src={jd} alt="user_picture" height="100" width="100" /></Link></td>
          <td>John Doe</td>
        </tr>
      </table>
    );
  }
}

export default Home;