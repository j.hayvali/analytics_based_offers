import React from 'react'
import { Link } from 'react-router-dom'
import './Header.css';
import settings from './settings.png';
import about from './about.png';

// The Header creates links that can be used to navigate
// between routes.
var liStyle = {
  float: 'right'
}
const Header = () => (
  <header className="header">
      <div className="titleDiv">
        <h1 className="title">Fahrzeugauswahl</h1>
      </div>
      <div className="navDiv">
    <nav>
      <ul className="nav">
        <li ><Link to='/'>Home</Link></li>
        <li ><Link to='/user'>User</Link></li>
        <li ><Link to='/statistiks'>Statistiks</Link></li>   
        <li style={liStyle}> <Link to='/about'><img src={about} height="20" width="30" /></Link></li>
        <li style={liStyle}> <Link to='/settings'><img src={settings} height="20" width="30"/></Link></li>
      </ul>
    </nav>
    </div>
  </header>
)

export default Header